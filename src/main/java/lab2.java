import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import  java.io.*;
import java.util.Scanner;

public class lab2 {
    public static void main(String[] args) throws IOException, ParseException {
        Scanner in = new Scanner(System.in);
        Calendar data = Calendar.getInstance();
        int year ;
        int month ;
        int date ;
        System.out.print("Введите год:");
        year = in.nextInt();
        System.out.print("Введите номер месяца:");
        month = in.nextInt()-1;
        System.out.print("Введите число:");
        date = in.nextInt();
        data.set(year,month,date);

        Calendar tempStart = Calendar.getInstance();
        Calendar tempEnd = Calendar.getInstance();
        int count=0;
        try(
                InputStream input = new FileInputStream("file.csv");
                Reader reader = new InputStreamReader(input);
                BufferedReader buf = new BufferedReader(reader);
                )
        {
            String line = "";
            String[] values;
            while((line = buf.readLine()) != null)
            {
                values=line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                try
                {
                    year = Integer.parseInt(values[10].substring(0, 4));
                    month = Integer.parseInt(values[10].substring(4, 6));
                    date = Integer.parseInt(values[10].substring(6, 8));

                    tempStart.set(year,month,date);

                    year = Integer.parseInt(values[11].substring(0, 4));
                    month = Integer.parseInt(values[11].substring(4, 6));
                    date = Integer.parseInt(values[11].substring(6, 8));

                    tempEnd.set(year,month,date);
                }
                catch (Exception e){/*System.out.println(e);*/}
                if(data.before(tempEnd)&&data.after(tempStart))count++;
            }
        }
            System.out.println(count+" дорожных ограничений действовало в городе на определенную дату.");
        }

}


